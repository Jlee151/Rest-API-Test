## Splunk Homework

<h2>How to get running</h2>
<ol>
    <li>Environment: Windows 10, Eclipse Java EE Oxygen 3, Java 1.8.0_151</li>
	<li>Download zip through web or command line and unzip.</li>
	<li>Open Eclipse and import existing Maven project.</li>
	<li>Import unzipped folder into Eclipse workspace.</li>
	<li>Expand folder tree and there should be a JRE System Library. Right click and make sure the execution environment is set to JavaSE-1.8.</li>
	<li>Right click root folder, go to Run As -> JUnit Test, then click on it.</li>
    <li>If any errors occur, maven clean then update project, and then go back to step 5.
    <li>Eclipse JUnit perspective will show test cases and their respective pass/fail.
	<li>Comments within the code should explain the methods and how I tested things.</li>
</ol>

<h2>Bugs found</h2>
<ul>
	<li>GET request for the "count" parameter does not work. Sample path like "/movies?q-=batman&count=2" does not return 2 movies. It returns all movies related to batman."</li>
    <li>POST request does not seem to work and this could be attributed to the server. Refer to "test_0_sendNewMovieInfo" in TestSplunkMovies_POST.java to see what I mean. Required fields are in the parameters with the correct content type, but when the assertion for the body's response checks the parameter, it's wrong. Related issues:</li>
    <ul>
        <li>Server accepts the body when any or all of the required fields are missing.</li>
        <li>Unsure of how null is accounted for (refer to test_3 and test_5) when it's a required parameter.</li>
        <li>Along with the main issue, still won't receive data with additional parameters.</li>
    </ul>
    <li>Based on current data, server should be checking for these problems:</li>
    <ul>
        <li>Duplicate poster paths for the movie image.</li>
        <li>Poster path links being invalid.</li>
        <li>Sorting responses by genre_ids then ascending id.</li>
        <li>Can release date be empty/null?</li>
        <li>Only one occurrence of backdrop path and it's null? This might not actually be a bug.</li>
        <li>This is for movies and I am assuming that the video field says if it is a movie or not, but some of them are false.</li>
    </ul>
    <li>The curl call example uses "name" and "description", but the json response is "title" and "overview", respectively. Tried interchanging them in POST request, but neither worked as intended.</li>
</ul>
