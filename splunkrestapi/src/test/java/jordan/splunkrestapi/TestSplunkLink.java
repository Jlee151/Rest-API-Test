package jordan.splunkrestapi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSplunkLink extends BaseTestSplunk {

	private JsonPath jsonResponse;
	private static int INITIAL_NUM_MOVIES = 16;
	private static int NUM_MOVIES_WANTED_IN_GET_REQUEST = 2;

	/**
	 * Testing the response's status code for requesting the server.
	 * 
	 * Test Status -> Pass
	 */
	@Test
	public void test_0_ValidStatusCode() {
		httpRequest.accept(ContentType.JSON);
		Response res = httpRequest.get("movies?q=batman");
		int statusCode = SplunkLinkMethods.checkStatusCode(res);
		assertEquals("Invalid status code.", 200, statusCode);
	}

	/**
	 * Testing a request's content type and what is acceptable.
	 * 
	 * Test Status -> Fail
	 */
	@Test
	public void test_1_WrongContentType() {
		ContentType ct = ContentType.TEXT;
		httpRequest.accept(ct);
		boolean requestCT = SplunkLinkMethods.checkContentType(ct);
		assertEquals("Content Type invalid.", true, requestCT);
	}

	/**
	 * Testing the response code with the wrong request.
	 * 
	 * Test Status -> Pass
	 */
	@Test
	public void test_2_WrongPath() {
		httpRequest.accept(ContentType.JSON);
		Response res = httpRequest.get("testPath?q=wow");
		int statusCode = SplunkLinkMethods.checkStatusCode(res);
		assertTrue("Valid status code for a GET request that was meant to be wrong.", statusCode != 200);
	}

	/**
	 * Testing the initial number of movies.
	 * 
	 * Depending on the other of JUnit tests being ran, the TestSplunkMovies_POST.java might run first
	 * and put a movie into the records. Played it safe by knowing the initial amount of movies and
	 * seeing if the number of movies retrieved it greater than or equal to the base amount.
	 * 
	 * Test Status -> Pass
	 */
	@Test
	public void test_3_CountNumMovies() {
		httpRequest.accept(ContentType.JSON);
		Response res = httpRequest.get("movies?q=batman");
		int numMovies = SplunkLinkMethods.getNumberOfMovies(res);
		assertTrue("Incorrect number of movies.", numMovies >= INITIAL_NUM_MOVIES);
	}

	/**
	 * Testing the optional "count=X" parameter in GET request
	 * 
	 * Test Status -> Fail
	 */
	@Test
	public void test_4_checkCountParametersInGET() {
		httpRequest.accept(ContentType.JSON);
		StringBuilder sb = new StringBuilder();
		sb.append("movies?q=batman&count=");
		sb.append(NUM_MOVIES_WANTED_IN_GET_REQUEST);
		Response res = httpRequest.get(sb.toString());
		jsonResponse = res.jsonPath();
		boolean numMoviesInJSON = SplunkLinkMethods.checkCountParameterWithJSON(jsonResponse,
				NUM_MOVIES_WANTED_IN_GET_REQUEST);
		assertTrue("Invalid number of movies in response.", numMoviesInJSON);
	}
}