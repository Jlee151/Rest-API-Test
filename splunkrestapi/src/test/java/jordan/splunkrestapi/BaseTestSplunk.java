package jordan.splunkrestapi;

import org.junit.AfterClass;
import org.junit.BeforeClass;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.parsing.Parser;
import com.jayway.restassured.specification.RequestSpecification;

public class BaseTestSplunk {
	
	public static RequestSpecification httpRequest = null;

	SplunkLinkMethods splunkLink = new SplunkLinkMethods();
	SplunkMovieMethods splunkMovie = new SplunkMovieMethods();

	@BeforeClass
	public static void setup() {
		RestAssured.baseURI = "http://splunk.mocklab.io";
		RestAssured.defaultParser = Parser.JSON;
		httpRequest = RestAssured.given();
	}

	@AfterClass
	public static void teardown() {
	}
}
