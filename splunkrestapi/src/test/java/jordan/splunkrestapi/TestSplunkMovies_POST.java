package jordan.splunkrestapi;

import org.json.JSONObject;
import org.junit.*;
import org.junit.runners.MethodSorters;
import static org.hamcrest.Matchers.equalTo;

import java.util.ArrayList;
import java.util.List;

import com.jayway.restassured.http.ContentType;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSplunkMovies_POST extends BaseTestSplunk {

	private String movieTitle = "catwoman fights crime";
	private String movieDescription = "a woman dressed as a cat does the impossible";

	/**
	 * Test required fields with right content type
	 * 
	 * Expected result -> Pass
	 * Test Status -> Pass
	 */
	@Test
	public void test_0_sendNewMovieInfo() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", movieTitle);
		jsonObject.put("description", movieDescription);
		httpRequest.header("Content-Type", ContentType.JSON).body(jsonObject.toString()).post("/movies").then()
				.statusCode(200).body("title", equalTo(movieTitle));
	}

	/**
	 * Test required fields with wrong content type
	 * 
	 * Expected result -> Pass
	 * Test Status -> Pass
	 */
	@Test
	public void test_1_sendNewMovieInfoWrongContentType() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", movieTitle);
		jsonObject.put("description", movieDescription);
		httpRequest.header("Content-Type", ContentType.TEXT).body(jsonObject.toString()).post("/movies").then()
				.statusCode(404);
	}

	/**
	 * Test without the "name" required field
	 * 
	 * Expected result -> Fail
	 * Test Status -> Pass
	 */
	@Test
	public void test_2_noNameField() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("description", movieDescription);
		httpRequest.header("Content-Type", ContentType.JSON).body(jsonObject.toString()).post("/movies").then()
				.statusCode(200);
	}

	/**
	 * Test with the "name" field being null
	 * 
	 * Expected result -> Fail
	 * Test Status -> Pass
	 */
	@Test
	public void test_3_nullNameField() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", JSONObject.NULL);
		jsonObject.put("description", movieDescription);
		httpRequest.header("Content-Type", ContentType.JSON).body(jsonObject.toString()).post("/movies").then()
				.statusCode(200);
	}

	/**
	 * Test without the "description" required field
	 * 
	 * Expected result -> Fail
	 * Test Status -> Pass
	 */
	@Test
	public void test_4_noDescriptionField() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", movieTitle);
		httpRequest.header("Content-Type", ContentType.JSON).body(jsonObject.toString()).post("/movies").then()
				.statusCode(200);
	}

	/**
	 * Test with the "description" field being null
	 * 
	 * Expected result -> Fail
	 * Test Status -> Pass
	 */
	@Test
	public void test_5_nullDescriptionField() {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("name", movieTitle);
		jsonObject.put("description", JSONObject.NULL);
		httpRequest.header("Content-Type", ContentType.JSON).body(jsonObject.toString()).post("/movies").then()
				.statusCode(200);
	}

	/**
	 * Test an empty body
	 * 
	 * Expected result -> Fail
	 * Test Status -> Pass
	 */
	@Test
	public void test_6_noFields() {
		JSONObject jsonObject = new JSONObject();
		httpRequest.header("Content-Type", ContentType.JSON).body(jsonObject.toString()).post("/movies").then()
				.statusCode(200);
	}

	/**
	 * Test to see if it's possible to add any other field along with the required
	 * ones
	 * 
	 * Expected result -> Pass
	 * Test Status -> Pass
	 */
	@Test
	public void test_7_additionalFields() {
		JSONObject jsonObject = new JSONObject();
		List<Integer> ids = new ArrayList<Integer>();
		ids.add(34);
		ids.add(500);
		ids.add(1233);
		jsonObject.put("name", movieTitle);
		jsonObject.put("description", movieDescription);
		jsonObject.put("genre_ids", ids);
		httpRequest.header("Content-Type", ContentType.JSON).body(jsonObject.toString()).post("/movies").then()
				.statusCode(200).body("genre_ids", equalTo(ids));
	}

}
