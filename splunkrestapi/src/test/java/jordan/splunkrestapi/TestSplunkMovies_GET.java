package jordan.splunkrestapi;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.*;
import org.junit.runners.MethodSorters;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSplunkMovies_GET extends BaseTestSplunk {

	private JsonPath jsonResponse;

	/**
	 * Testing that there is a response
	 * 
	 * Expected result -> Pass
	 * Test Status -> Pass
	 */
	@Test
	public void test_0_GrabJSONData() {
		httpRequest.accept(ContentType.JSON);
		Response res = httpRequest.get("movies?q=batman");
		jsonResponse = res.jsonPath();
	}

	/**
	 * Testing if movie images or poster paths are unique
	 * 
	 * Expected result -> Pass
	 * Test Status -> Fail
	 */
	@Test
	public void test_1_PosterDuplicates() {
		httpRequest.accept(ContentType.JSON);
		Response res = httpRequest.get("movies?q=batman");
		jsonResponse = res.jsonPath();
		boolean posterDuplicates = SplunkMovieMethods.containsMoviePosterDuplicates(jsonResponse);
		assertEquals("Poster duplicates.", posterDuplicates, true);
	}

	/**
	 * Testing if poster paths are valid links
	 * 
	 * Expected result -> Pass
	 * Test Status -> Fail
	 */
	@Test
	public void test_2_ValidPosterLinks() {
		httpRequest.accept(ContentType.JSON);
		Response res = httpRequest.get("movies?q=batman");
		jsonResponse = res.jsonPath();
		boolean validPosterPaths = SplunkMovieMethods.checkValidPosterLink(jsonResponse);
		assertTrue("Invalid poster path found.", validPosterPaths);
	}

	/**
	 * Testing if there is a palindrome in the title
	 * 
	 * Expected result -> Pass
	 * Test Status -> Pass
	 */
	@Test
	public void test_3_TitlePalindromes() {
		httpRequest.accept(ContentType.JSON);
		Response res = httpRequest.get("movies?q=batman");
		jsonResponse = res.jsonPath();
		boolean palindromeFound = SplunkMovieMethods.containsMovieTitlePalindrome(jsonResponse);
		assertEquals("Title Palindrome Not Found.", true, palindromeFound);
	}

	/**
	 * Testing to see if the json response is sorted by "id" and "genre_ids" properly
	 * 
	 * Expected result -> Pass
	 * Test Status -> Fail
	 */
	@Test
	public void test_4_SortingGenreId() {
		httpRequest.accept(ContentType.JSON);
		Response res = httpRequest.get("movies?q=batman");
		jsonResponse = res.jsonPath();
		boolean sortedResponse = SplunkMovieMethods.checkSortedGenreIds(jsonResponse);
		assertEquals("Title Palindrome Not Found.", true, sortedResponse);
	}

	/**
	 * Testing to see if the number of movies with their "genre_ids" sum greater than 400 is at most 7
	 * 
	 * Expected result -> Pass
	 * Test Status -> Pass
	 */
	@Test
	public void test_5_SumGenreId() {
		httpRequest.accept(ContentType.JSON);
		Response res = httpRequest.get("movies?q=batman");
		jsonResponse = res.jsonPath();
		int numMoviesWithSumGenreId = SplunkMovieMethods.checkGenreIdSum(jsonResponse);
		assertTrue("Incorrect number of movies with sum(genre_id) > 400", numMoviesWithSumGenreId <= 7);
	}

	/**
	 * Testing whether or not a movie title contains the title of another
	 * 
	 * Expected result -> Pass
	 * Test Status -> Pass
	 */
	@Test
	public void test_6_TitleWithinTitle() {
		httpRequest.accept(ContentType.JSON);
		Response res = httpRequest.get("movies?q=batman");
		jsonResponse = res.jsonPath();
		int numOfTitleInceptions = SplunkMovieMethods.checkTitleWithinOtherTitle(jsonResponse);
		assertTrue("Number of Titles Containing Another Title is Less Than 2.", numOfTitleInceptions >= 2);
	}

	/**
	 * Testing to see if movie ratings are within the bounds of [0, 10]
	 * 
	 * Expected result -> Pass
	 * Test Status -> Pass
	 */
	@Test
	public void test_7_RealRatings() {
		httpRequest.accept(ContentType.JSON);
		Response res = httpRequest.get("movies?q=batman");
		jsonResponse = res.jsonPath();
		boolean areRatingsValid = SplunkMovieMethods.checkRatings(jsonResponse);
		assertTrue("Incorrect rating was found.", areRatingsValid);
	}

	/**
	 * Testing to see if the required fields for a POST request are always present
	 * 
	 * Expected result -> Pass
	 * Test Status -> Pass
	 */
	@Test
	public void test_8_nonEmptyTitleAndDescription() {
		httpRequest.accept(ContentType.JSON);
		Response res = httpRequest.get("movies?q=batman");
		jsonResponse = res.jsonPath();
		boolean actualTitleAndDescription = SplunkMovieMethods.checkTitleAndOverview(jsonResponse);
		assertTrue("A movie missing a title and/or description.", actualTitleAndDescription);
	}

}