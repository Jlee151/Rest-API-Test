package jordan.splunkrestapi;

import java.util.List;

public final class IDs {

	private Integer movieId;
	private List<Integer> genreIds;

	public IDs(Integer movieId, List<Integer> list) {
		this.movieId = movieId;
		this.genreIds = list;
	}

	public Integer getMovieId() {
		return movieId;
	}

	public List<Integer> getGenreIds() {
		return genreIds;
	}

	public String toString() {
		return movieId + " <-> " + genreIds;
	}

}
