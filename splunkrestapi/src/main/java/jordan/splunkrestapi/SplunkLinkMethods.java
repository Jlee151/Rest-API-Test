package jordan.splunkrestapi;

import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.path.json.JsonPath;
import com.jayway.restassured.response.Response;

import java.util.ArrayList;
import java.util.List;

public class SplunkLinkMethods {

	private static JsonPath jpath = null;

	/**
	 * @param res response from server
	 * 
	 * @return response status code
	 */
	public static int checkStatusCode(Response res) {
		return res.statusCode();
	}
	
	/**
	 * @param ct ContentType of the request
	 * 
	 * @return boolean that determines if the content type is of the JSON style
	 */
	public static boolean checkContentType(ContentType ct) {
		return ct.equals(ContentType.JSON);
	}

	/**
	 * @param res response from server
	 * 
	 * @return number of json entries from the server response
	 */
	public static int getNumberOfMovies(Response res) {
		jpath = res.jsonPath();
		return new ArrayList<String>(jpath.getList("results")).size();
	}
	
	/**
	 * @param json response from server
	 * @param count number of json entries expected from GET request
	 * 
	 * @return boolean that determines if the right count was returned
	 */
	public static boolean checkCountParameterWithJSON(JsonPath json, int count) {
		List<Object> movieInfoList = json.setRoot("results").get();
		return count == movieInfoList.size();
	}

}
