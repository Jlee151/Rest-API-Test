package jordan.splunkrestapi;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.jayway.restassured.path.json.JsonPath;

public class SplunkMovieMethods {

	/**
	 * SPL-001: unique movie poster paths aka movie image
	 * 
	 * Try storing poster path string into a set. If it's already in the set, it is not unique and would
	 * return false for all movie images being unique.
	 * 
	 * @param json response from the server
	 * 
	 * @return boolean that determines if there are duplicate poster paths
	 */
	public static boolean containsMoviePosterDuplicates(JsonPath json) {
		List<String> movieImageList = json.setRoot("results").getList("poster_path");
		Set<String> uniqueMoviePosters = new HashSet<String>();
		for (String s : movieImageList) {
			if (!uniqueMoviePosters.add(s) && s != null) {
				return false;
			}
		}
		return true;
	}

	/**
	 * SPL-002: valid poster paths including null
	 * 
	 * Store list of strings of poster paths and calls the helper function isValidURL(poster_path) to
	 * determine if it's a valid link.
	 * 
	 * @param json response from the server
	 * 
	 * @return boolean that determines whether or not all poster paths present are valid
	 */
	public static boolean checkValidPosterLink(JsonPath json) {
		List<String> posterPathList = json.setRoot("results").getJsonObject("poster_path");
		for (String s : posterPathList) {
			if (!isValidURL(String.valueOf(s))) {
				return false;
			}
		}
		return true;
	}

	/**
	 * SPL-002 helper function
	 * 
	 * Tries constructing a URL then URI out of the link. Corner case of null is not the actual null,
	 * but rather a string value of "null" because of how the json response was and the fact that there
	 * would be a null pointer exception if the function argument was the actual null.
	 * 
	 * @param posterURL potentially valid link
	 * 
	 * @return boolean that determines if the link was successful created into a URI
	 */
	private static boolean isValidURL(String posterURL) {
		try {
			if (posterURL.equals("null")) {
				return true;
			} else {
				new URL(posterURL).toURI();
				return true;
			}
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * SPL-005: movie title containing a palindrome
	 * 
	 * Business requirements never fully defined what they want in a palindrome. Should special
	 * characters be considered? Any minimum length? Examples of a palindrome currenty found in json
	 * response is "Kayak" and "A".
	 * 
	 * Decided on using a regex to replace all non-alphanumeric strings and place all words separated by
	 * a whitspace into a list. Parse that list and individually check the potential word palindrome.
	 * 
	 * @param json response from server
	 * 
	 * @return boolean that determines if a movie title does contain a palindrome
	 */
	public static boolean containsMovieTitlePalindrome(JsonPath json) {
		List<String> movieTitleList = json.setRoot("results").getList("title");
		List<String> movieTitleWordsList = new ArrayList<String>();
		boolean palindromeFound = false;
		for (String s : movieTitleList) {
			String title = String.valueOf(s);
			String[] strArr = title.replaceAll("[^A-Za-z0-9 ]", "").split(" ");
			movieTitleWordsList.addAll(Arrays.asList(strArr));
		}
		for (String word : movieTitleWordsList) {
			if (word.toLowerCase().equals(new StringBuffer(word.toLowerCase()).reverse().toString())) {
				palindromeFound = true;
			}
		}
		return palindromeFound;
	}

	/**
	 * SPL-003: sort by any null genre id then ascending id
	 * 
	 * Get respective lists of "id" and "genre_ids" and make a new list of an object called "IDs" to
	 * store the json element's "id" and "genre_ids".
	 * 
	 * @param json response from server
	 * 
	 * @return boolean that determines if json response is in the right order
	 */
	public static boolean checkSortedGenreIds(JsonPath json) {
		List<Integer> movieIdList = json.setRoot("results").getList("id");
		List<List<Integer>> genreIdList = json.setRoot("results").getList("genre_ids");
		List<IDs> mapIdToGenreIdList = new ArrayList<IDs>();
		List<IDs> comparisonList = new ArrayList<IDs>();
		for (int i = 0; i < movieIdList.size(); i++) {
			mapIdToGenreIdList.add(new IDs(movieIdList.get(i), genreIdList.get(i)));
			comparisonList.add(new IDs(movieIdList.get(i), genreIdList.get(i)));
		}
		List<IDs> sortedMapIdToGenreIdList = sortByGenreIdAndId(comparisonList);
		return sortedMapIdToGenreIdList.equals(mapIdToGenreIdList);
	}

	/**
	 * SPL-003 helper function
	 * 
	 * Sort the list by IDs.getMovieId first to maintain the ascending "id" status then sort by
	 * IDs.getGenreIds. Sorting by "genre_ids" involving having to check if the list containing all or
	 * none of the "genre_ids" was null or empty. Null or empty "genre_ids" had priority and if multiple
	 * null or empty "genre_ids", it's subsorted by ascending id. That is why we sorted by "id" first.
	 * 
	 * Had to override the compare function for the Comparator to work with the custom object IDs
	 * 
	 * @param list list of IDs to sort
	 * 
	 * @return list of IDs that have been sorted by priority of genre_id and ascending id
	 */
	private static List<IDs> sortByGenreIdAndId(List<IDs> list) {
		List<IDs> sortedList = new ArrayList<IDs>();
		list.sort(Comparator.comparing(IDs::getMovieId));
		list.sort(new Comparator<IDs>() {
			@Override
			public int compare(IDs id1, IDs id2) {
				if (id1.getGenreIds() == null || id1.getGenreIds().isEmpty()) {
					return -1;
				} else if (id2.getGenreIds() == null || id2.getGenreIds().isEmpty()) {
					return 1;
				} else {
					return 0;
				}
			}
		});
		sortedList = list;
		return sortedList;
	}

	/**
	 * SPL-004: number of movies with sum(genre_id) > 400 can at most be 7
	 * 
	 * Grab list of "genre_ids" and sums it up. Calls the checkListSum(genre_id_list) helper function to
	 * sum up the list and return true if the sum is greater than 400.
	 * 
	 * @param json response from server
	 * 
	 * @return number of movies with sum(genre_id) > 400
	 */
	public static int checkGenreIdSum(JsonPath json) {
		List<List<Integer>> genreIdList = json.setRoot("results").getList("genre_ids");
		int totalNumMoviesWithGenreIdSumGreaterThan400 = 0;
		for (List<Integer> list : genreIdList) {
			if (checkListSum(list)) {
				totalNumMoviesWithGenreIdSumGreaterThan400++;
			}
		}
		return totalNumMoviesWithGenreIdSumGreaterThan400;
	}

	/**
	 * SPL-004 helper function
	 * 
	 * Classic loop summing up elements in list. Given that the provided data was not that extensive, a
	 * simple loop accomplishes the task, but in the event that there is a ton more data, Java streams
	 * might be more optimal so replace the entire loop with the following code -
	 * genreIds.stream().mapToInt(Integer::intValue).sum().
	 * 
	 * Now if the response is really big, add parallel() after stream().
	 * 
	 * @param genreIds list containing all the genreIds associated with a movie
	 * 
	 * @return boolean that determines if the sum of the list is greater than 400
	 */
	private static boolean checkListSum(List<Integer> genreIds) {
		int sumOfIds = 0;
		for (Integer i : genreIds) {
			sumOfIds += i;
		}
		return sumOfIds > 400 ? true : false;
	}

	/**
	 * SPL-006: title within another title
	 * 
	 * Grabs movie titles and uses a regex to remove trailing and leading special characters. May not be
	 * a necessary step depending on how the comparison loop(s) are implemented.
	 * 
	 * Currently I have a slightly slower implementation, but there might be a better solution by
	 * storing all words split by whitespace into a hashtable. From the list of stripped down
	 * characters, loop through, make a string array from the respective index, then loop through the
	 * string array and see if any element is in the hashtable.
	 * 
	 * @param json response from server
	 * 
	 * @return number of occurrences where a movie title is in another title
	 */
	public static int checkTitleWithinOtherTitle(JsonPath json) {
		List<String> movieTitleList = json.setRoot("results").getList("title");
		List<String> movieListStrippedDown = new ArrayList<String>();
		int titleSubTitleOccurrences = 0;
		for (int i = 0; i < movieTitleList.size(); i++) {
			String str = movieTitleList.get(i).replaceAll("^\\p{P}+|\\p{P}+$|", "").replaceAll("[^A-Za-z0-9\\'-]+", " ")
					.toLowerCase();
			movieListStrippedDown.add(str);
		}
		for (int i = 0; i < movieListStrippedDown.size(); i++) {
			String movie1 = movieListStrippedDown.get(i);
			for (int j = i + 1; j < movieListStrippedDown.size(); j++) {
				String movie2 = movieListStrippedDown.get(j);
				if (movie1 != movie2 && (movie1.contains(movie2) || movie2.contains(movie1))) {
					titleSubTitleOccurrences++;
				}
			}
		}
		return titleSubTitleOccurrences;
	}

	/**
	 * Ratings Test Case
	 * 
	 * Assuming that there ratings are out of 10, grab a list of objects that will be casted as a double
	 * to be made sure it is between 0 and 10. Trying to use a list of floats resulted in errors with
	 * checking the value.
	 * 
	 * @param json response from server
	 * 
	 * @return boolean that determines if there is an invalid rating number
	 */
	public static boolean checkRatings(JsonPath json) {
		List<Object> ratingsList = json.setRoot("results").getList("vote_average");
		for (Object o : ratingsList) {
			if (Double.valueOf(String.valueOf(o)) < 0 || Double.valueOf(String.valueOf(o)) > 10) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Movie Title and Description Test Case
	 * 
	 * Check list of movie overviews aka descriptions for actual content. Making a POST request involves
	 * having a movie title and description so it would only seem fair that all current movies on the
	 * server have a title and description.
	 * 
	 * @param json response from server
	 * 
	 * @return boolean that determines if a movie has an overview aka description
	 */
	public static boolean checkTitleAndOverview(JsonPath json) {
		List<String> titleList = json.setRoot("results").getList("title");
		List<String> overviewList = json.setRoot("results").getList("overview");
		for (String title : titleList) {
			if (title.trim().isEmpty() || title.equals(null) || title.equals("null")) {
				return false;
			}
		}
		for (String description : overviewList) {
			if (description.trim().isEmpty() || description.equals(null) || description.equals("null")) {
				return false;
			}
		}
		return true;
	}

}
